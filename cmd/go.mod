module gitlab.com/irfanpatel/firewalllogparser

go 1.13

require (
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/hpcloud/tail v1.0.0
	github.com/streadway/amqp v0.0.0-20200108173154-1c71cc93ed71
	gitlab.com/irfanpatel/mrmonitor v0.0.0-20200421102207-a2559d2c2332
	gitlab.com/irfanpatel/persistor v0.0.0-20200422033331-5c0ed3479ce4 // indirect
	gopkg.in/fsnotify.v1 v1.4.7 // indirect
	gopkg.in/tomb.v1 v1.0.0-20141024135613-dd632973f1e7 // indirect
)
