package main

import (
	"encoding/json"
	"fmt"
	"github.com/hpcloud/tail"
	"github.com/streadway/amqp"
	"gitlab.com/irfanpatel/mrmonitor/models"
	"gitlab.com/irfanpatel/persistor"
	"net"
	"strconv"
	"strings"
	"time"
)

//get the local time offset to attach to firewall timestamp as it does not contain the offset
var localTime = []byte(time.Now().Format(time.RFC3339))
var localOffset = string(localTime[19:])
var queue amqp.Queue
var channel *amqp.Channel


func main() {
	t, err := tail.TailFile("C:\\Windows\\System32\\LogFiles\\Firewall\\pfirewall.log", tail.Config{Follow: true, ReOpen: true, Poll: true})
	queue,channel = persistor.CreateAMQPInstance()

	if err != nil {
		fmt.Println("error")
	}
	for line := range t.Lines {
		converted := ParseLine(line.Text)
		formatted, err := json.Marshal(converted)
		if err == nil && string(formatted) != "null" {
			sendMessage(formatted)
			fmt.Println(string(formatted))
		}


	}
}

func IsPrivateIP(ip string) bool {
	var privateIPBlocks []*net.IPNet

	_, private24BitIPBlocks, _ := net.ParseCIDR("10.0.0.0/8")
	_, private20BitIPBlocks, _ := net.ParseCIDR("172.16.0.0/12")
	_, private16BitIPBlocks, _ := net.ParseCIDR("192.168.0.0/16")

	privateIPBlocks = append(privateIPBlocks, private24BitIPBlocks)
	privateIPBlocks = append(privateIPBlocks, private20BitIPBlocks)
	privateIPBlocks = append(privateIPBlocks, private16BitIPBlocks)

	IPaddress := net.ParseIP(ip)

	if IPaddress.IsLoopback() || IPaddress.IsLinkLocalUnicast() || IPaddress.IsLinkLocalMulticast() {
		return true

	}

	for _, block := range privateIPBlocks {
		if block.Contains(IPaddress) {
			return true
		}
	}

	return false
}

func ParseLine(logData string) *models.Logdata {

	//Check if line is a header line, log line will start with a date
	// hence the first digit will be a number
	_, err := strconv.Atoi(logData[:1])

	//proceed if the line is not a header line
	if err == nil {
		logLine := strings.Split(logData, " ")
		logLineLength := len(logLine)

		//Ignore private ip's
		if logLineLength == 17 && !IsPrivateIP(logLine[5]) {
			lookupResult, _ := net.LookupAddr(logLine[5])
			var dnsName string
			if len(lookupResult) > 0 {
				dnsName = lookupResult[0]
			}
			return &models.Logdata{
				Datetime:        logLine[0] + "T" + logLine[1] + localOffset,
				Action:          logLine[2],
				Protocol:        logLine[3],
				SourceIP:        logLine[4],
				DestinationIP:   logLine[5],
				DNSName:         dnsName,
				SourcePort:      logLine[6],
				DestinationPort: logLine[7],
				MessageSize:     logLine[8],
				Path:            logLine[15],
			}
		}
	}

	return nil
}

func sendMessage(message []byte){
	err := channel.Publish(
		"",
		queue.Name,
		false,
		false,
		amqp.Publishing{
			ContentType: "application/json",
			Body: message,
		})
	if err != nil {
		fmt.Println("Error sending message")
	}
}
